const Titles = document.querySelector("h1");
var x = document.getElementsByTagName("UL");
var h2 = document.querySelector("h2");
var body = document.getElementsByTagName("BODY");
document.body.style.textAlign = "center";
h2.style.textDecoration = "underline";
var myVar = setInterval(myTimer, 1000);
var myVars = setInterval(myTimers, 1000);

const titles = document.querySelectorAll("li");
titles.forEach((title, i) => {
  const done = new RegExp("Done");
  const Ongoing = new RegExp("Ongoing");
  console.log(title);
  console.log(done.test(title));
  console.log(Ongoing.test(title));
  if (done.test(title)) {
    title.style.backgroundColor = "green";
  } else if (Ongoing.test(title)) {
    title.style.backgroundColor = "yellow";
  } else {
    title.style.backgroundColor = "red";
  }
});

function getRandomColor() {
  var letters = "0123456789ABCDEF";
  var color = "#";
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}
function myTimers() {
  Titles.style.color = "" + getRandomColor();
}
function myTimer() {
  var strArray = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];

  var d = new Date();
  var datestring =
    strArray[d.getMonth()] +
    " " +
    d.getDate() +
    ", " +
    d.getFullYear() +
    " " +
    d.getHours() +
    ":" +
    d.getMinutes() +
    ":" +
    d.getSeconds();
  document.getElementById("demo").innerHTML = datestring;
  document.getElementById("demo").style.backgroundColor = "" + getRandomColor();
}
