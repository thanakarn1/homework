console.log("Day 21 Exercises: Level 1-1");
const p = document.querySelector("p");
console.log(p);

console.log("Day 21 Exercises: Level 1-2");
const id = document.querySelector("#first-title");
console.log(id);

console.log("Day 21 Exercises: Level 1-3");
const h1 = document.querySelectorAll("h1");
console.log(h1);

console.log("Day 21 Exercises: Level 1-4");
const allTitles = document.querySelectorAll("h1");
for (let i = 0; i < allTitles.length; i++) {
  console.log(allTitles[i]);
}

console.log("Day 21 Exercises: Level 1-5");
const titles = document.querySelectorAll("h1");

titles[3].textContent = "Fourth Title";

console.log("Day 21 Exercises: Level 1-6");
titles[3].setAttribute("class", "title");
titles[3].setAttribute("id", "fourth-title");

console.log("Day 21 Exercises: Level 2-1");
titles[3].style.color = "#ffff";
titles[3].style.backgroundColor = "green";
titles[3].style.border = "1px solid";
const size = "60px";
titles[3].style.font = size;
const family = "Arial";
titles[3].style.font = family;

console.log("Day 21 Exercises: Level 2-2");
titles.forEach((title, i) => {
  if (i % 2 === 0) {
    title.style.color = "red";
  } else {
    title.style.color = "green";
  }
});

console.log("Day 21 Exercises: Level 2-3");
titles[3].setAttribute("class", "title");
titles[3].setAttribute("id", "fourth-title");
