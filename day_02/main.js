let values = '30 Days Of JavaScript'
console.log(values);

//Level1-3
console.log(values.length);
//Level1-4
console.log(values.toUpperCase());
//Level1-5
console.log(values.toLowerCase());
//Level1-6
console.log(values.substring(0,3));
//Level1-7
console.log(values.slice(3));
//Level1-8
console.log(values.includes('Script'));
//Level1-9
console.log(values.split());
//Level1-10
console.log(values.split(' '));
//Level1-11
let str = 'Facebook, Google, Microsoft, Apple, IBM, Oracle, Amazon'
console.log(str.split(','));
//Level1-12
console.log(values.replace('JavaScript','Python'));
//Level1-13
console.log('Level1-13 = '+values.charAt(15));
//Level1-14
console.log('Level1-14 = '+values.charCodeAt(11));
//Level1-15
console.log('Level1-15 = '+values.indexOf('a'));
//Level1-16
console.log('Level1-16 = '+values.lastIndexOf('a'));
//Level1-17
console.log('Level1-17 = '+values.indexOf('30'));
//Level1-18
console.log('Level1-18 = '+values.lastIndexOf('JavaScript'));
//Level1-19
console.log('Level1-19 = '+values.search('30'));
//Level1-20
let str_trim =' 30 Days Of JavaScript ' 
console.log('Level1-20 = '+str_trim.trim(' '));
//Level1-21
console.log('Level1-21 = '+values.startsWith('30'));
//Level1-22
console.log('Level1-22 = '+values.endsWith('Script'));
//Level1-23
console.log('Level1-23 = '+values.match('a'));
//Level1-24
let str_concat = '30 Days Of'
console.log('Level1-24 = '+str_concat.concat(' JavaScript'));
//Level1-25
console.log('Level1-25 = '+values.repeat(2));

//-----------------Level 2----------------------
let str_levelTwo = 'There is no exercise better for the heart than reaching down and lifting people up.'
console.log(str_levelTwo);
console.log("Love is not patronizing and charity isn't about pity, it is about love. Charity and love are the same -- with charity you give love, so don't just give money but reach out your hand instead.");

//Level2-3
let value = '10'
if(typeof value === 10){
    console.log(typeof value);
}else{
    console.log('Before convert = ' + typeof value);
    value =  Number(value)
    console.log('After convert = ' + typeof value);
}
//Level2-4
let float = '9.8'
if(float === 10 ){

}else{
    console.log(parseInt(float)+1);
}
//Level2-5

//Level2-7
let randomOne = Math.floor(Math.random()*101) 
console.log('randomOne = '+randomOne);
let randomTwo = Math.floor(Math.random()*101)+50
console.log('randomTwo = '+randomTwo);
let randomThree = Math.floor(Math.random()*256)
console.log('randomThree = '+randomThree);
//Level2-10
let result
let str_random ='JavaScript'
let arr_str = str_random.length

for ( var i = 0; i < 5; i++ ) {
    result += str_random.charAt(Math.floor(Math.random() * arr_str));
 }
 console.log(result);
 
//Level2-11

//Level2-12
let senT = 'You cannot end a sentence with because because because is a conjunction'
console.log('senT = '+senT.substr(30,24));


//------------------Level 3------
//level3-1
let string_levelThree = 'Love is the best thing in this world. Some found their love and some are still looking for their love.'
let regEX = /love/gi
console.log(string_levelThree.match(regEX).length);


//level3-2
let str_count = 'You cannot end a sentence with because because because is a conjunction'
let regExTwo = /because/gi
console.log(str_count.match(regExTwo));
//level3-3
const sentence = '%I $am@% a %tea@cher%, &and& I lo%#ve %te@a@ching%;. The@re $is no@th@ing; &as& mo@re rewarding as educa@ting &and& @emp%o@weri@ng peo@ple. ;I found tea@ching m%o@re interesting tha@n any ot#her %jo@bs. %Do@es thi%s mo@tiv#ate yo@u to be a tea@cher!? %Th#is 30#Days&OfJavaScript &is al@so $the $resu@lt of &love& of tea&ching'
let str_spe = /[^$%#@&;!]/gi
let str_T =''
let str_arr_spe =sentence.match(str_spe)
str_arr_spe.map(e =>{
    str_T += '' +e
    
})
console.log(str_T);



//level3-3
const sentence_cal = 'He earns 5000 euro from salary per month, 10000 euro annual bonus, 15000 euro online courses per month.'
let regEx_cal = /\d+/g
let arr_cal = sentence_cal.match(regEx_cal)
let num =0
arr_cal.forEach( data => {
   return  num+=Number(data)
})
console.log(num);







































