const countries = [
  ["Finland", "Helsinki"],
  ["Sweden", "Stockholm"],
  ["Norway", "Oslo"],
];
const names = ["Asabeneh", "Brook", "David", "John"];
console.table("Ex1-1");
console.table(names);
console.table("Ex1-2");
console.table(countries);
console.group("Names");
console.log(names);
console.groupEnd();

console.assert(10 > 2 * 10, "10 is greater than 2*10");
console.warn("This is a warning");
console.error("This is an error message");

while (i < 10) {
  console.count("While has been called");
}

for (i = 0; i < 5; i++) {
  console.count("for has been called");
}

const array1 = ["a", "b", "c"];

for (const element of array1) {
  console.count("for of has been called");
}

const array1 = ["a", "b", "c"];

array1.forEach((element) => console.count("forEach has been called"));
