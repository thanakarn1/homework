const pattern = /\d+/g; // d is a special character which means digits
const txt =
  "He earns 4000 euro from salary per month, 10000 euro annual bonus, 5500 euro online courses per month.";
var matches = txt.match(pattern);
var total = 0;
for (const property in matches) {
  total += Number(matches[property]);
  console.log(matches[property]);
}
console.log(total);

points = ["-1", "2", "-4", "-3", "-1", "0", "4", "8"];
sortedPoints = [-4, -3, -1, -1, 0, 2, 4, 8];
distance = 12;

console.log(matches);

const pattern = /-|[0-9]/; // this square bracket mean either A or a
const txt1 = "first_name";
const txt2 = "first-name";
const txt3 = "1first_name";
const txt4 = "firstname";
const matches1 = !pattern.test(txt1);
const matches2 = !pattern.test(txt2);
const matches3 = !pattern.test(txt3);
const matches4 = !pattern.test(txt4);

console.log("is_valid_variable(" + txt1 + ") = #" + matches1);
console.log("is_valid_variable(" + txt2 + ") = #" + matches2);
console.log("is_valid_variable(" + txt3 + ") = #" + matches3);
console.log("is_valid_variable(" + txt4 + ") = #" + matches4);

const paragraph =
  "I love teaching. If you do not love teaching what else can you love. I love Python if you do not love something which can give you all the capabilities to develop an application what else can you love.";

function tenMostFrequentWords(paragraph) {
  const p = paragraph.replace(/\./g, "");
  const message = p.split(" ");
  var count = {};
  message.forEach(function (i) {
    count[i] = (count[i] || 0) + 1;
  });

  var arr = [];
  for (var prop in count) {
    if (count.hasOwnProperty(prop)) {
      arr.push({
        word: prop,
        count: count[prop],
      });
    }
  }
  arr.sort(function (a, b) {
    return b.count - a.count;
  });
  //arr.sort(function(a, b) { a.value.toLowerCase().localeCompare(b.value.toLowerCase()); }); //use this to sort as strings
  arr.length = 10;
  return arr;
}
console.log(tenMostFrequentWords(paragraph));

const paragraph =
  "%I $am@% a %tea@cher%, &and& I lo%#ve %tea@ching%;. There $is nothing; &as& mo@re rewarding as educa@ting &and& @emp%o@wering peo@ple. ;I found tea@ching m%o@re interesting tha@n any other %jo@bs. %Do@es thi%s mo@tivate yo@u to be a tea@cher!?";
function cleanText(message) {
  message = message.replace(/[%@#$&;!?.,]/g, "");
  return message;
}
function MostFrequentWords(paragraph) {
  const p = paragraph.replace(/\./g, "");
  const message = p.split(" ");
  var count = {};
  message.forEach(function (i) {
    count[i] = (count[i] || 0) + 1;
  });

  var arr = [];
  for (var prop in count) {
    if (count.hasOwnProperty(prop)) {
      arr.push({
        word: prop,
        count: count[prop],
      });
    }
  }
  arr.sort(function (a, b) {
    return b.count - a.count;
  });

  return arr;
}
console.log(cleanText(paragraph));
console.log(MostFrequentWords(cleanText(paragraph)));
