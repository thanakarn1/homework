console.log("Day 15 Exercises: Level 2-1")

class Animals  {
  constructor(name, age, color, legs) {
    this.name = name
    this.age = age
    this.color = color
    this.legs = legs
  }
  getName() {
    const Name = this.name 
    return Name
  }
}

class Dogs extends Animals {
  constructor(name, age, color, legs , gender) {
    super(name, age, color, legs)
    this.gender = gender
  }
getDog() {
    const Names ="My dog name is " + this.name +","+ this.age +" years old."+"It is "+ this.color +","+"It is "+ this.legs+" leg"; 
    
    return Names
  }
}
const dogs = new Dogs('dog', 5, 'Gray', 4,'m')
console.log(dogs.getDog())
