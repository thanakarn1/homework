console.log("Day 15 Exercises: Level 1-1")

class Animal  {
  constructor(name, age, color, legs) {
    this.name = name
    this.age = age
    this.color = color
    this.legs = legs
  }
  getName() {
    const Name = this.name 
    return Name
  }
}

const animal1 = new Animal('Jame', 5, 'Gray', 4)
const animal2 = new Animal('Fray', 11, 'Green', 6)

console.log(animal1.getName())
console.log(animal2.getName())

console.log("Day 15 Exercises: Level 1-2")

class Dog extends Animal {
  constructor(name, age, color, legs , gender) {
    super(name, age, color, legs)
    this.gender = gender
  }
getDog() {
    const Names = this.name + this.age + this.color + this.legs + this.gender
    return Names
  }
}
const dog = new Dog('dog', 5, 'Gray', 4,'m')
console.log(dog.getDog())

class Cat extends Animal {
  constructor(name, age, color, legs , gender) {
    super(name, age, color, legs)
    this.gender = gender
  }
getCat() {
    const Names = this.name + this.age + this.color + this.legs + this.gender
    return Names
  }
}
const cat = new Cat('cat', 4, 'Gray', 2,'m')
console.log(cat.getCat())