console.log("Day 15 Exercises: Level 3-1")
class statistics  {
  constructor() {
  }
  static count() {
	const ages = [31, 26, 34, 37, 27, 26, 32, 32, 26, 27, 27, 24, 32, 33, 27, 25, 26, 38, 37, 31, 34, 24, 33, 29, 26]
    return ages.length;
  }
  static sum() {
	const ages = [31, 26, 34, 37, 27, 26, 32, 32, 26, 27, 27, 24, 32, 33, 27, 25, 26, 38, 37, 31, 34, 24, 33, 29, 26]
    var total = 0;
    for (const p in ages) {
	 total += Number(ages[p]);
    }
    return total
  }
  static min() {
	const ages = [31, 26, 34, 37, 27, 26, 32, 32, 26, 27, 27, 24, 32, 33, 27, 25, 26, 38, 37, 31, 34, 24, 33, 29, 26]
    ages.sort(function(a, b){return a - b});
    return ages[0];
  }
  static max() {
	const ages = [31, 26, 34, 37, 27, 26, 32, 32, 26, 27, 27, 24, 32, 33, 27, 25, 26, 38, 37, 31, 34, 24, 33, 29, 26]
    ages.sort(function(a, b){return a - b});
    return ages[ages.length-1];
  }
  static range() {
	const ages = [31, 26, 34, 37, 27, 26, 32, 32, 26, 27, 27, 24, 32, 33, 27, 25, 26, 38, 37, 31, 34, 24, 33, 29, 26]
    ages.sort(function(a, b){return a - b});
    return ages[ages.length-1]-ages[0];
  }
  static mean() {
	const ages = [31, 26, 34, 37, 27, 26, 32, 32, 26, 27, 27, 24, 32, 33, 27, 25, 26, 38, 37, 31, 34, 24, 33, 29, 26]
    var total = 0;
    for (const p in ages) {
	 total += Number(ages[p]);
    }
    return (total/ages.length).toFixed();
  }

  static median() {
	const ages = [31, 26, 34, 37, 27, 26, 32, 32, 26, 27, 27, 24, 32, 33, 27, 25, 26, 38, 37, 31, 34, 24, 33, 29, 26]
    ages.sort(function(a, b){return a - b});
	var position = ages.length
	var median = 0
	if(position%2 == 0){
		median = (ages[position/2]+ages[(position+2)/2])/2
	}else{
		median = ages[(ages.length+1)/2]
	}
    return median;
  }

  static mode() {
	const ages = [31, 26, 34, 37, 27, 26, 32, 32, 26, 27, 27, 24, 32, 33, 27, 25, 26, 38, 37, 31, 34, 24, 33, 29, 26]
	var modes = [], count = [], i, number, maxIndex = 0,c;
 
    for (i = 0; i < ages.length; i += 1) {
        number = ages[i];
        count[number] = (count[number] || 0) + 1;
        if (count[number] > maxIndex) {
            maxIndex = count[number];
        }
    }
    for (i in count)
        if (count.hasOwnProperty(i)) {
            if (count[i] === maxIndex) {
                modes.push(Number(i));
				c = count[i]
            }
        }
	var m = {'mode': modes[0], 'count': c}
    return m;
  }

  static var() {
	const ages = [31, 26, 34, 37, 27, 26, 32, 32, 26, 27, 27, 24, 32, 33, 27, 25, 26, 38, 37, 31, 34, 24, 33, 29, 26]
	var i,total = 0
	var mean = this.mean();
	for(i = 0 ; i < ages.length ; i++){
	ages[i] = ages[i]-mean
	ages[i] = ages[i]*ages[i]
	total += ages[i]
	}
return total/ages.length ;
  }

  static std() {
	const ages = [31, 26, 34, 37, 27, 26, 32, 32, 26, 27, 27, 24, 32, 33, 27, 25, 26, 38, 37, 31, 34, 24, 33, 29, 26]
	var i,total = 0
	var mean = this.mean();
	for(i = 0 ; i < ages.length ; i++){
	ages[i] = ages[i]-mean
	ages[i] = ages[i]*ages[i]
	total += ages[i]
	}
return Math.sqrt(total/(ages.length-1)).toFixed (2) ;
  }

static freqDist() {
	const ages = [31, 26, 34, 37, 27, 26, 32, 32, 26, 27, 27, 24, 32, 33, 27, 25, 26, 38, 37, 31, 34, 24, 33, 29, 26]
	var modes = [], count = [], i, number, maxIndex = 0,c;
 
    for (i = 0; i < ages.length; i += 1) {
        number = ages[i];
        count[number] = (count[number] || 0) + 1;
        if (count[number] > maxIndex) {
            maxIndex = count[number];
        }
    }
    for (i in count)
        if (count.hasOwnProperty(i)) {
            if (count[i] === maxIndex) {
                modes.push(Number(i));
				c = count[i]
            }
        }
    return count;
  }
}
console.log('Count:', statistics.count()) // 25
console.log('Sum: ', statistics.sum()) // 744
console.log('Min: ', statistics.min()) // 24
console.log('Max: ', statistics.max()) // 38
console.log('Range: ', statistics.range()) // 14
console.log('Mean: ', statistics.mean()) // 30
console.log('Median: ',statistics.median()) // 29
console.log('Median: ',statistics.mode()) // 29
console.log('Variance: ',statistics.var()) // 17.5
console.log('Standard Deviation: ', statistics.std()) // 4.2
console.log('Frequency Distribution: ',statistics.freqDist())