var rows = 17;
var cols = 6;
createTable(rows, cols);
function createTable(rows, cols) {
  var j = 1;
  var num = 0;
  var output = "<table border='1' width='500' cellspacing='0'cellpadding='5'>";
  for (i = 1; i <= rows; i++) {
    output = output + "<tr>";
    while (j <= cols) {
      if (num % 2 == 0 || num % 3 == 0 || num % 5 == 0 || num % 7 == 0) {
        if (num == 2 || num == 3 || num == 5 || num == 7) {
          output = output + "<td style='background-color:red'>" + num + "</td>";
        } else {
          if (num % 2 == 1) {
            output =
              output + "<td style='background-color:yellow'>" + num + "</td>";
          } else {
            output =
              output + "<td style='background-color:green'>" + num + "</td>";
          }
        }
      } else {
        if (num == 1) {
          output =
            output + "<td style='background-color:yellow'>" + num + "</td>";
        } else {
          output = output + "<td style='background-color:red'>" + num + "</td>";
        }
      }
      j = j + 1;

      num++;
    }
    output = output + "</tr>";
    j = 1;
  }
  output = output + "</table>";
  document.write(output);
}
