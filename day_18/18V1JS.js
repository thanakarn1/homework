const countriesAPI = 'https://restcountries.eu/rest/v2/all'
const catsAPI = 'https://api.thecatapi.com/v1/breeds'

console.log("Day 18 Exercises: Level 1-1")


fetch(countriesAPI)
  .then(response => response.json()) 
  .then(data => { 
    console.log(data)
  })
  .catch(error => console.log(error)) 
