
console.log("Day 18 Exercises: Level 3-1")

fetch(catsAPI)
  .then(response => response.json())
  .then(data => {
    console.log(data)
  })
  .catch(error => console.log(error))
console.log("Day 18 Exercises: Level 3-2")

fetch(countriesAPI)
  .then(response => response.json())
  .then(data => {
    console.log(data)
  })
  .catch(error => console.log(error))
console.log("Day 18 Exercises: Level 3-3")

const fetchData = async () => {
  try {
    const response = await fetch(countriesAPI)
    const countries = await response.json()
    console.log(countries)
  } catch (err) {
    console.log(err)
  }
}
console.log('===== async and await')
fetchData()