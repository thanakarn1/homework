
console.log("Day 17 Exercises: Level 3-1")

let personAccount = [
  { firstname: 'A',lastname: 'AA',incomes: 10,expenses : 5 },
  { firstname: 'B',lastname: 'BB',incomes: 200,expenses : 40 },
  { firstname: 'C',lastname: 'CC',incomes: 1040,expenses : 50 },
  { firstname: 'D',lastname: 'DD',incomes: 150,expenses : 800 },
  { firstname: 'E',lastname: 'EE',incomes: 1040,expenses : 42 },
  { firstname: 'G',lastname: 'GG',incomes: 1055,expenses : 85 },
]

let personAccounts = JSON.stringify(personAccount)
localStorage.setItem('personAccount', personAccounts)

console.log(localStorage)