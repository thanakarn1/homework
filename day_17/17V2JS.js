

console.log("Day 17 Exercises: Level 2-1")

let student = {
  firstName:'Asabeneh',
  lastName:'Yetayehe',
  age:25,
  country:'Thailand',
  enrolled:'Maejo',
  skills:['HTML', 'CSS', 'JS', 'React','Node', 'Python', ]
}
const userText = JSON.stringify(student, undefined, 4)
localStorage.setItem('student', userText)
console.log(localStorage)