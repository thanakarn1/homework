const constants = [2.72, 3.14, 9.81, 37, 100];
// const countries = ["Finland", "Estonia", "Sweden", "Denmark", "Norway"];
const rectangle = {
  width: 20,
  height: 10,
  area: 200,
  perimeter: 60,
};
const users = [
  {
    name: "Brook",
    scores: 75,
    skills: ["HTM", "CSS", "JS"],
    age: 16,
  },
  {
    name: "Alex",
    scores: 80,
    skills: ["HTM", "CSS", "JS"],
    age: 18,
  },
  {
    name: "David",
    scores: 75,
    skills: ["HTM", "CSS"],
    age: 22,
  },
  {
    name: "John",
    scores: 85,
    skills: ["HTML"],
    age: 25,
  },
  {
    name: "Sara",
    scores: 95,
    skills: ["HTM", "CSS", "JS"],
    age: 26,
  },
  {
    name: "Martha",
    scores: 80,
    skills: ["HTM", "CSS", "JS"],
    age: 18,
  },
  {
    name: "Thomas",
    scores: 90,
    skills: ["HTM", "CSS", "JS"],
    age: 20,
  },
];

// let { width, height, area, perimeter } = rectangle;
// console.log(width, height, area, perimeter);

// let [fin, est, sw, den, nor] = countries;

// for (const { width, height, area, perimeter } of rectangle) {
//   console.log(width, height, area, perimeter);
// }

// for (const { name, scores, skills, age } of users) {
//   console.log(name, scores, skills, age);
// }
// const calculatePerimeter = (user) => {
//   for (const u of user) {
//     if (u.skills.length > 2) {
//       return u;
//     }
//   }
// };

// console.log(calculatePerimeter(users)); // 60

// for (const { name, capital, population, languages } of countries) {
//   console.log(name, capital, population, languages);
// }

// const student = [["David"], ["HTM", "CSS", "JS", "React"], [98, 85, 90, 95]];

// for (const [name, skills, jsScore, reactScore] of student) {
//   console.log(name, skills, jsScore, reactScore);
// }
// console.log(name, skills, jsScore, reactScore);

const students = [
  ["David", ["HTM", "CSS", "JS", "React"], [98, 85, 90, 95]],
  ["John", ["HTM", "CSS", "JS", "React"], [85, 80, 85, 80]],
];
function convertArrayToObject(u) {
  const entries = new Map(u);
  //console.log("convertArrayToObject -> entries", entries);

  // const obj = Object.fromEntries(entries);

  console.log(entries);
}
convertArrayToObject(students);

const student = {
  name: "David",
  age: 25,
  skills: {
    frontEnd: [
      { skill: "HTML", level: 10 },
      { skill: "CSS", level: 8 },
      { skill: "JS", level: 8 },
      { skill: "React", level: 9 },
    ],
    backEnd: [
      { skill: "Node", level: 7 },
      { skill: "GraphQL", level: 8 },
    ],
    dataBase: [{ skill: "MongoDB", level: 7.5 }],
    dataScience: ["Python", "R", "D3.js"],
  },
};

student.skills.frontEnd.push({ skill: "BootStrap", level: 8 });
student.skills.backEnd.push({ skill: "Express", level: 9 });
student.skills.dataBase.push({ skill: "SQL", level: 8 });
student.skills.dataScience.push("SQL");

console.log(student);
