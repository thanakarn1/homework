const a = [4, 5, 8, 9];
const b = [3, 4, 5, 7];
//const countries = ["Finland", "Sweden", "Norway"];
const c_em = new Set();
for (let index = 0; index < 11; index++) {
  c_em.add(index);
}
console.log(c_em);
c_em.delete(2);
console.log("c_em", c_em);
c_em.clear();
console.log("c_em", c_em);
const companies = ["Google", "Facebook", "Amazon", "Oracle", "Microsoft"];
companies.forEach((e) => {
  c_em.add(e);
});
console.log(c_em);

const m = new Map();
companies.forEach((e) => {
  m.set(e, e.length);
});
console.log(m);
console.log([...a, ...b]);
let A = new Set(a);
let B = new Set(b);

let c = a.filter((num) => B.has(num));
let C = new Set(c);
console.log(C);

let D_A = new Set(a);
let D_B = new Set(b);

let d_c = a.filter((num) => !B.has(num));
let D_C = new Set(d_c);

console.log(D_C);
console.log(countries.length);
//console.log(Object.entries(countries));
const copyCounties = Object.assign({}, countries);

const entries = Object.entries(copyCounties);
const counts = [];
const arr_count = [];
function mostSpokenLanguages(c, num) {
  c.forEach((e) => {
    var l = e[1].languages;
    arr_count.push(...l);
    //s.add(arr_count);
  });
  const s = new Set(arr_count);
  //console.log(s);

  for (const l of s) {
    const filteredLang = arr_count.filter((lng) => lng === l);
    //console.log(filteredLang); // ["English", "English", "English"]
    counts.push({ lang: l, count: filteredLang.length });
  }

  for (let index = 0; index < num; index++) {
    console.log(counts[index]);
  }
}
mostSpokenLanguages(entries, 3);
