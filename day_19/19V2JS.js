

console.log("Day 19 Exercises: Level 2-1")

function outerFunctions() {
    let count = 0;
    function innerFunction() {
        count++
        return count
    }

    return innerFunction
}
const innerFunct = outerFunctions()

console.log(innerFunct())
console.log(innerFunct())
console.log(innerFunct())