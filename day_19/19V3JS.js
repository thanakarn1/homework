
console.log("Day 19 Exercises: Level 3-1")
class person  {
  constructor(firstname, lastname, incomes, expenses) {
    this.firstname = firstname
    this.lastname = lastname
    this.incomes = incomes
    this.expenses = expenses
  }
}


function personAccount() {
	const person1 = new person('Jame', 'son', 1000, 400)
    let personAccount = 0;
    function firstname() {
        return person1.firstname
    }
    function lastname() {
        return person1.lastname
    }
    function incomes() {
        return person1.incomes
    }
    function expenses() {
        return person1.expenses
    }

    return {
        firstname:firstname(),
        lastname:lastname(),
        incomes:incomes(),
        expenses:expenses()
    }
}
const personAccount = personAccount()

console.log(personAccount.firstname)
console.log(personAccount.lastname)
console.log(personAccount.incomes)
console.log(personAccount.expenses)