const mernStack = ["MongoDB", "Express", "React", "Node"];

//Level1-1
// let i=0
// for (let index = 0; index < 11; index++) {
//     console.log(i++);
// }
// while (i<11) {
//     //console.log(i++);
// }
// do {
//     console.log(i);
// i++
// } while (i<11);
//Level1-2
// let i=10
// for (let index = 10; index >= 0; index--) {
//     console.log(index);
// }
// while (i>=0) {
//     console.log(i--);
// }
// do {
//     console.log(i);
// i--
// } while (i>=0);
//Level1-3
// let n =3
// for (let index = 0; index <= n; index++) {
//     console.log(index++);
// }
//Level1-4
// let str=''
// for (let i = 0; i < 8; i++) {
//     for (let j = 0; j <i; j++) {
//         str+='#'
//     }
//     console.log(str);
//     str=''
// }
//Level1-5
// for (let i = 0; i < 11; i++) {
//     console.log(`${i} x ${i} = ${i*i}`);
// }
//Level1-6
// for (let i = 0; i < 11; i++) {
//     console.log(`${i} ${Math.pow(i,2)} ${Math.pow(i,3)} `);
// }
//Level1-7 , 1-8
// var i=1
// do {
//     if(i%2 == 0){
//         console.log('Even number = ',i);
//     }else{
//         console.log('Odd number = ',i);

//     }
//     i++

// } while (i<=100);
//Level1-9
// let n = 100;

// for (let i = 2; i <= n; i++) {

//   for (let j = 2; j < i; j++) {
//     if (i % j == 0) console.log('Not Prime');
//     ;
//   }
//   console.log(i);
// }
//Level1-10
// var i=0
// var sum =0
// while (i<101) {
//     sum+=i
//     i++
// }
// console.log(`The sum of all numbers from 0 to 100 is ${sum}.`);
//Level1-11 , 1-12
// var i=0
// var sum_evens =0
// var sum_odd =0
// var arr_sum=[]
// while (i<101) {
//     if(i%2 == 0){
//         sum_evens+=i
//     }else{
//         sum_odd+=i
//     }
//     i++
// }
// arr_sum.push(sum_evens)
//     arr_sum.push(sum_odd)
// console.log(`The sum of all numbers from 0 to 100 is ${sum_evens}. And the sum of all odds from 0 to 100 is ${sum_odd}.`);
// console.log(arr_sum);

//Level1-13 , 1-14
var arr = [];
while (arr.length < 5) {
  var r = Math.floor(Math.random() * 100) + 1;
  if (arr.indexOf(r) === -1) arr.push(r);
}
console.log(arr);
//Level1-15
var result = "";
var characters =
  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
var charactersLength = characters.length;
for (var i = 0; i < 6; i++) {
  result += characters.charAt(Math.floor(Math.random() * charactersLength));
}
console.log(result);

//Level2-4
// countries = new Array("ALBANIA", "BOLIVIA", "CANADA", "DENMARK", "ETHIOPIA", "FINLAND", "GERMANY", "HUNGARY", "IRELAND", "JAPAN", "KENYA");
// console.log(countries);
//Level2-5
// var arr = []
// countries.forEach(element => {
// arr.push(element.length)

// });
// console.log(arr);
//Level2-6 Skip
//Level2-7
const asw = countries.filter((word) => word.toLowerCase().includes("land"));
console.log(asw);
//Level2-8
const asw_iva = countries.filter((word) => word.toLowerCase().includes("ia"));
console.log(asw_iva);
//Level2-10
const asw_cha = countries.filter((word) => word.length <= 5);
console.log(asw_cha);
//Level2-11
var str_max = "";
for (let index = 0; index < webTechs.length; index++) {
  if (webTechs[0].length < webTechs[index].length) {
    str_max = webTechs[index];
  }
}
console.log(str_max);

//Level2-12
var str_arr = [];
webTechs.forEach((e) => {
  str_arr.push([e, e.length]);
});
console.log("b", str_arr);

//Level2-13 Skip
//Level2-14 Skip
//Level2-15
const fruit = ["banana", "orange", "mango", "lemon"];
console.log(fruit.reverse());
//Level2-16
const fullStack = [
  ["HTML", "CSS", "JS", "React"],
  ["Node", "Express", "MongoDB"],
];
var arr_fullstack = fullStack[0].concat(fullStack[1]);
arr_fullstack.forEach((e) => {
  console.log(e);
});
//Level 3
const countries_copy = countries;
console.log(countries_copy);
//level3-2
const sortCountries = countries_copy.sort();
console.log(sortCountries);
//Level3-3
console.log(webTechs.sort());
console.log(mernStack.sort());
//Level3-4
const c_aws = sortCountries.filter((word) =>
  word.toLowerCase().includes("land")
);
console.log(c_aws);
//level3-5
var str_max = "";
for (let index = 0; index < sortCountries.length; index++) {
  if (sortCountries[0].length < sortCountries[index].length) {
    str_max = sortCountries[index];
  }
}
console.log(str_max);

//level3-6
const c_aws_six = sortCountries.filter((word) =>
  word.toLowerCase().includes("land")
);
console.log(c_aws_six);
//level3-7
const c_aws_seven = sortCountries.filter((word) => word.length <= 4);
console.log(c_aws_seven);
//level3-8
const c_aws_two = sortCountries.filter((word) => word.length <= 2);
console.log(c_aws_two);
//level3-9
const c_aws_reverse = sortCountries.reverse();
console.log(c_aws_reverse);
